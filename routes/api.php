<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


//-------------------------check user login is invalid ----------------------------
Route::post('checkLogin', [
    'uses' => 'Auth\AuthController@checkLogin'
]);





//-------------------------saveCompanyData----------------------------
Route::post('saveCompanyData', [
    'uses' => 'APIController@saveCompanyData'
]);

//-------------------------saveCompanyimports----------------------------
Route::post('saveCompanyimports', [
    'uses' => 'APIController@saveCompanyimports'
]);

//-------------------------saveCompanyexports----------------------------
Route::post('saveCompanyexports', [
    'uses' => 'APIController@saveCompanyexports'
]);

//-------------------------saveCompanydecla----------------------------
Route::post('saveCompanydecla', [
    'uses' => 'APIController@saveCompanydecla'
]);

