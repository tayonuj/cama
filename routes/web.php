                                                                                                                                                                                                                                                     <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;





Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/application', 'HomeController@application');
Route::get('/applicationb', 'HomeController@applicationb');
Route::get('/applicationc', 'HomeController@applicationc');
Route::get('/applicationd', 'HomeController@applicationd');
Route::get('/applicatione', 'HomeController@applicatione');
Route::get('/help', 'HomeController@help');
Route::get('/applicationbo', 'HomeController@applicationbo');
Route::get('/applicationf', 'HomeController@applicationf');
Route::get('/approval', 'HomeController@approval');
Route::get('/rejectapp', 'HomeController@rejectapp');
Route::get('/dashboardmodsc', 'HomeController@dashboardmodsc');
Route::get('/scnewapplication', 'HomeController@scnewapplication');
Route::get('/approved', 'HomeController@approved');
Route::get('/rejected', 'HomeController@rejected');
Route::get('/applicationmodsca', 'HomeController@applicationmodsca');
Route::get('/applicationmodscb', 'HomeController@applicationmodscb');
Route::get('/applicationmodscc', 'HomeController@applicationmodscc');
Route::get('/applicationmodscd', 'HomeController@applicationmodscd');
Route::get('/applicationmodsce', 'HomeController@applicationmodsce');
Route::get('/applicationmodscf', 'HomeController@applicationmodscf');
Route::get('/applicationmodscg', 'HomeController@applicationmodscg');
Route::get('/newapplication', 'HomeController@newapplication');
Route::get('/applicationboia', 'HomeController@applicationboia');
Route::get('/applicationboib', 'HomeController@applicationboib');
Route::get('/applicationboic', 'HomeController@applicationboic');
Route::get('/applicationboid', 'HomeController@applicationboid');
Route::get('/applicationboie', 'HomeController@applicationboie');
Route::get('/applicationboif', 'HomeController@applicationboif');
Route::get('/newapplicationdashboard', 'HomeController@newapplicationdashboard');
Route::get('/approvedboi', 'HomeController@approvedboi');
Route::get('/rejectedboi', 'HomeController@rejectedboi');
Route::get('/scnewapplicationdb', 'HomeController@scnewapplicationdb');
Route::get('/memos', 'HomeController@memos');
Route::get('/boia', 'HomeController@boia');
Route::get('/asnewapplicationdb', 'HomeController@asnewapplicationdb');
Route::get('/approvedas', 'HomeController@approvedas');
Route::get('/rejectedas', 'HomeController@rejectedas');
Route::get('/applicationasa', 'HomeController@applicationasa');
Route::get('/applicationasb', 'HomeController@applicationasb');
Route::get('/applicationasc', 'HomeController@applicationasc');
Route::get('/applicationasd', 'HomeController@applicationasd');
Route::get('/applicationase', 'HomeController@applicationase');
Route::get('/applicationasf', 'HomeController@applicationasf');
Route::get('/applicationasg', 'HomeController@applicationasg');
Route::get('/applicationash', 'HomeController@applicationash');
Route::get('/asnewapplication', 'HomeController@asnewapplication');
Route::get('/ministry/{id}', 'HomeController@organization');
Route::get('/dataCollect', 'HomeController@dataCollect');
//---------------------------------------Block Page----------------------------------------
Route::get('/blocked', 'HomeController@blocked')->name('blocked');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
