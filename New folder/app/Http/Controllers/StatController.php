<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Farm;
use App\Farmer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class StatController extends Controller
{
    public function getStat(){
        $dataset = new stdClass();
        $ministry_count = Employee::where('category','=','Ministry')->count();
        $health_count = Employee::where('category','=','Health')->count();
        $education_count = Employee::where('category','=','Education')->count();
        $total_count = Employee::count();
        $dataset->ministry_count = $ministry_count;
        $dataset->health_count = $health_count;
        $dataset->education_count = $education_count;
        $dataset->total_farmers = $total_count;

        return response()->json(['http_status'=>'success','dataset'=>$dataset]);
    }
}
