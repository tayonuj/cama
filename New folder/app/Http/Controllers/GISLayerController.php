<?php

namespace App\Http\Controllers;

use App\Airports;
use App\Anicut;
use App\Archeologicals;
use App\AutoMobiles;
use App\Banks;
use App\Canal;
use App\CommTowers;
use App\Employee;
use App\FoodsNBeverage;
use App\FualStations;
use App\GovInstitutes;
use App\Harbers;
use App\Healths;
use App\LandUsePoints;
use App\LeptoAddresses;
use App\LifeStyles;
use App\PublicPlaces;
use App\ReligiousPlaces;
use App\Soils;
use App\Supermarkets;
use App\Tank;
use App\TempEmployee;
use App\Transpotations;
use Illuminate\Http\Request;

class GISLayerController extends Controller
{
    //-------------------------------------GIS Layers from DSD GND-------------------------------------------------------------------
    public function getGISLayerByBoundaryType(Request $request){
        $layer =  $request->input('layer');
        $location_type =  $request->input('location_type');
        $location =  $request->input('location');
        $sub_locations = null;
        $data = null;

        switch ($layer){
            case 'anicut' :
                switch ($location_type){
                    case 'district' : $sub_locations =  Anicut::whereIn('district',$location)->groupBy('DO_Div')->pluck('DO_Div')->toArray();break;
                    case 'dsd' :  $sub_locations =  Anicut::whereIn('DO_Div',$location)->groupBy('Village')->pluck('Village')->toArray();break;
                }
                break;

            case 'canals' :
                switch ($location_type){
                    case 'district' : $sub_locations =  Canal::whereIn('district',$location)->groupBy('DO_Div')->pluck('DO_Div')->toArray();break;
                    case 'dsd' :  $sub_locations =  [];break;
                }
                break;

            case 'tanks' :
                switch ($location_type){
                    case 'district' : $sub_locations =  Tank::whereIn('district',$location)->groupBy('DO_Div')->pluck('DO_Div')->toArray();break;
                    case 'dsd' :  $sub_locations =  Tank::whereIn('DO_Div',$location)->groupBy('Village')->pluck('Village')->toArray();break;
                }
                break;
            case 'LeptoAddress' :
                switch ($location_type){
                    case 'district' : $sub_locations =  LeptoAddresses::whereIn('district',$location)->groupBy('dsd')->pluck('dsd')->toArray();break;
                    case 'dsd' :  $sub_locations =  LeptoAddresses::whereIn('dsd',$location)->groupBy('gnd')->pluck('gnd')->toArray();break;
                }
                break;
            case 'Employee_Permanent' :
                switch ($location_type){
                    case 'district' : $sub_locations =  TempEmployee::whereIn('permanent_district',$location)->groupBy('permanent_dsd')->pluck('permanent_dsd')->toArray();break;
                    case 'dsd' :  $sub_locations =  TempEmployee::whereIn('permanent_dsd',$location)->groupBy('permanent_gnd')->pluck('permanent_gnd')->toArray();break;
                }
                break;
            case 'Employee_Home' :
                switch ($location_type){
                    case 'district' : $sub_locations =  TempEmployee::whereIn('present_district',$location)->groupBy('present_dsd')->pluck('present_dsd')->toArray();break;
                    case 'dsd' :  $sub_locations =  TempEmployee::whereIn('present_dsd',$location)->groupBy('present_gnd')->pluck('present_gnd')->toArray();break;
                }
                break;
        }

        return response()->json(['http_status'=>'success','requested_location'=>$location,'requested_location_type'=>$location_type,'sub_locations'=>$sub_locations]);

    }


    //-------------------------------------GIS Layers Data-------------------------------------------------------------------
    public function getGISLayerData(Request $request){
        $districts =  $request->input('districts');
        $dsds =  $request->input('dsds');
        $gnds =  $request->input('gnds');
        $location_type =  $request->input('location_type');
        $layer =  $request->input('layer');
        $data = null;


        switch ($layer){
            case 'canals' :
                switch ($location_type){
                    case 'district' : $data =  Canal::whereIn('district',$districts)->get();break;
                    case 'dsd' :  $data =  Canal::whereIn('district',$districts)->whereIn('DO_Div',$dsds)->get();break;
                    case 'gnd' :  $data =  Canal::whereIn('district',$districts)->whereIn('DO_Div',$dsds)->whereIn('Village',$gnds)->get();break;
                }
                break;

            case 'anicut' :
                switch ($location_type){
                    case 'district' : $data =  Anicut::whereIn('district',$districts)->get();break;
                    case 'dsd' :  $data =  Anicut::whereIn('district',$districts)->whereIn('DO_Div',$dsds)->get();break;
                    case 'gnd' :  $data =  Anicut::whereIn('district',$districts)->whereIn('DO_Div',$dsds)->whereIn('Village',$gnds)->get();break;
                }
                break;

            case 'tanks' :
                switch ($location_type){
                    case 'district' : $data =  Tank::whereIn('district',$districts)->get();break;
                    case 'dsd' :  $data =  Tank::whereIn('district',$districts)->whereIn('DO_Div',$dsds)->get();break;
                    case 'gnd' :  $data =  Tank::whereIn('district',$districts)->whereIn('DO_Div',$dsds)->whereIn('Village',$gnds)->get();break;
                }
                break;
            case 'LeptoAddress' :
                switch ($location_type){
                    case 'district' : $data =  LeptoAddresses::whereIn('district',$districts)->get();break;
                    case 'dsd' :  $data =  LeptoAddresses::whereIn('district',$districts)->whereIn('dsd',$dsds)->get();break;
                    case 'gnd' :  $data =  LeptoAddresses::whereIn('district',$districts)->whereIn('dsd',$dsds)->whereIn('gnd',$gnds)->get();break;
                }
                break;
            case 'Employee_Home' :
                switch ($location_type){
                    case 'district' : $data =  TempEmployee::select('present_latitude as lat' , 'present_longitude as lng','Email','Permanent_Add','Contact_No1')->whereIn('present_district',$districts)->get();break;
                    case 'dsd' :  $data =  TempEmployee::select('present_latitude as lat' , 'present_longitude as lng','Email','Permanent_Add','Contact_No1')->whereIn('present_district',$districts)->whereIn('present_dsd',$dsds)->get();break;
                    case 'gnd' :  $data =  TempEmployee::select('present_latitude as lat' , 'present_longitude as lng','Email','Permanent_Add','Contact_No1')->whereIn('present_district',$districts)->whereIn('present_dsd',$dsds)->whereIn('present_gnd',$gnds)->get();break;
                }
                break;

            case 'Employee_Permanent' :
                switch ($location_type){
                    case 'district' : $data =  TempEmployee::select('permanent_latitude as lat' , 'permanent_longitude as lng','Email','Permanent_Add','Contact_No1')->whereIn('permanent_district',$districts)->get();break;
                    case 'dsd' :  $data =  TempEmployee::select('permanent_latitude as lat' , 'permanent_longitude as lng','Email','Permanent_Add','Contact_No1')->whereIn('permanent_district',$districts)->whereIn('permanent_dsd',$dsds)->get();break;
                    case 'gnd' :  $data =  TempEmployee::select('permanent_latitude as lat' , 'permanent_longitude as lng','Email','Permanent_Add','Contact_No1')->whereIn('permanent_district',$districts)->whereIn('permanent_dsd',$dsds)->whereIn('permanent_gnd',$gnds)->get();break;
                }
                break;

        }

        return response()->json(['http_status'=>'success','data'=>$data]);

    }
}
