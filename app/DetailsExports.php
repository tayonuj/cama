<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailsExports extends Model
{
    use SoftDeletes;
    protected $table = 'details_exports';
}
