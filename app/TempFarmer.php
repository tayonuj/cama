<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TempFarmer extends Model
{
    use SoftDeletes;
    protected $table = 'temp_farmer';
}
