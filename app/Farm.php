<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Farm extends Model
{
    use softDeletes;
    public function farmer(){
        return $this->belongsTo('App\Farmer');
    }
}
