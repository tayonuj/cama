<?php

namespace App\Http\Controllers;

use App\Project;
use App\projects_details;
use Illuminate\Http\Request;
use ZipStream\Exception;


class AdminController extends Controller
{
    public function getProjects(Request $request){
        try {
            $data = projects_details::all();
            return response()->json(['http_status'=>'success','data'=>$data]);
        } catch (\Exception $e){
            return response($e->getMessage());
        }
    }

    public function getProjectsByID(Request  $request){
        $id = $request->input('id');
        $data = projects_details::where('id','=',$id)->first();
        $project_list = projects_details::all();
        return response()->json(['http_status'=>'success','data'=>$data,'project_list'=>$project_list]);
    }

    public function updateProjectStageLevel(Request  $request){
        $id = $request->input('proid');
        $stage_level = $request->input('stagelevel');
        if($stage_level=="0"){
            //pending status 1
            projects_details::where('id','=',$id)->update(['stage_level' => 1,'status'=>'pending']);
            return response()->json(['http_status'=>'success']);

        }elseif($stage_level=="1"){
                //assing sport officer have to change pending stage level 2//
        }elseif($stage_level=="2"){
            // submitted have to change pendin stage level 3 mobile//
        }elseif($stage_level=="3"){
            //Pending stage level 4 //
            projects_details::where('id','=',$id)->update(['stage_level' => 4,'status'=>'pending']);
            return response()->json(['http_status'=>'success']);

        }elseif($stage_level=="4"){
            //Pending stage level 5 //
            projects_details::where('id','=',$id)->update(['stage_level' => 5,'status'=>'pending']);
            return response()->json(['http_status'=>'success']);


        }elseif($stage_level=="5"){
            //ongoing stage level 6 //
            projects_details::where('id','=',$id)->update(['stage_level' => 6,'status'=>'ongoing']);
            return response()->json(['http_status'=>'success']);


        }else {
            # code...
        }

        // $data = Project::where('id','=',$id)->first();
        // $project_list = Project::all();
        // return response()->json(['http_status'=>'success','data'=>$data,'project_list'=>$project_list]);
    }
}
