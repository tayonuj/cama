<?php

namespace App\Http\Controllers;
use App\CompanyDetails;
use App\CompanyDirector;
use App\ContactUs;
use App\DetailsExports;
use App\DetailsImportsLocal;
use App\Employee;
use App\EmployeeStaff;
use App\Farmer;
use App\Mail\OfficerWelcome;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\TempFarmer;

class APIController extends Controller
{
    //-----------------------------save company data---------------------
    public function saveCompanyData(Request $request){
        $app_no = $request->input('app_no');
        $name = $request->input('name');
        $user_id = $request->input('user_id');
        $c_address = $request->input('c_address');
        $dira_name = $request->input('dira_name');
        $dira_mobile = $request->input('dira_mobile');
        $dirb_name = $request->input('dirb_name');
        $dirb_mobile = $request->input('dirb_mobile');
        $sm_name = $request->input('sm_name');
        $sm_pnum = $request->input('sm_pnum');
        $reg_no = $request->input('reg_no');
////------------------------Details of imports--------------------------------
//        $goods_des = $request->input('goods_des');
//        $t_from = $request->input('t_from');
//        $t_through = $request->input('t_through');
//        $t_to = $request->input('t_to');


        $company = new CompanyDetails();
        $company->app_no = $app_no;
        $company->c_name = $name;
        $company->c_address = $c_address;
        $company->sm_name = $sm_name;
        $company->sm_pnum = $sm_pnum;
        $company->c_regno = $reg_no;
        $company->created_by = $user_id;
        $company->save();

        $companydira = new CompanyDirector();
        $companydira->dir_name = $dira_name;
        $companydira->dir_pnum = $dira_mobile;
        $companydira->created_by = $user_id;
        $companydira->save();

        $companydirb = new CompanyDirector();
        $companydirb->dir_name = $dirb_name;
        $companydirb->dir_pnum = $dirb_mobile;
        $companydirb->created_by = $user_id;
        $companydirb->save();

//        $companyimports = new DetailsImportsLocal();
//        $companyimports->app_no = $app_no;
//        $companyimports->goods_des = $goods_des;
//        $companyimports->t_from = $t_from;
//        $companyimports->t_through = $t_through;
//        $companyimports->t_to = $t_to;
//        $companyimports->created_by = $user_id;
//        $companyimports->save();

        return response()->json(['http_status'=>'success','message'=>'company saved successfully','company'=>$company
        ]);

    }

//------------------------Details of imports--------------------------------
    public function saveCompanyimports(Request $request){
//        $app_no = $request->input('app_no');
        $user_id = $request->input('user_id');
        $goods_des = $request->input('goods_des');
        $t_from = $request->input('t_from');
        $t_through = $request->input('t_through');
        $t_to = $request->input('t_to');
        $company_id = $request->input('company_id');
        $invoice_no = $request->input('invoice_no');
        $ratio = $request->input('ratio');
        $quantity = $request->input('quantity');


        $companyimports = new DetailsImportsLocal();
        $companyimports->description = $goods_des;
        $companyimports->invoice_no = $invoice_no;
        $companyimports->ratio = $ratio;
        $companyimports->quantity = $quantity;
        $companyimports->created_by = $user_id;
        $companyimports->company_id = $company_id;
        $companyimports->save();

        $company = CompanyDetails::find($company_id);
        if ($company) {
            $company->t_from = $t_from;
            $company->t_through = $t_through;
            $company->t_to = $t_to;
            $company->created_by = $user_id;

            $company->save();
        }
        return response()->json(['http_status'=>'success','message'=>'company saved successfully','companyimports'=>$companyimports
        ]);

    }

    public function saveContactus(Request $request){
//        $app_no = $request->input('app_no');
        $con_name = $request->input('con_name');
        $user_id = $request->input('user_id');
        $e_mail = $request->input('e_mail');
        $number = $request->input('number');
        $message = $request->input('message');



        $companycon = new ContactUs();
//        $companycon->app_no = $app_no;
        $companycon->con_name = $con_name;
        $companycon->e_mail = $e_mail;
        $companycon->number = $number;
        $companycon->message = $message;
        $companycon->created_by = $user_id;
        $companycon->save();



        return response()->json(['http_status'=>'success','message'=>'company saved successfully','company'=>$companycon
        ]);

    }
//----------------------------------------------saveCompanyexports--------------------------------

    public function saveCompanyexports(Request $request){
        $finish_des = $request->input('finish_des');
        $user_id = $request->input('user_id');
        $finish_qua = $request->input('finish_qua');
        $finish_weight = $request->input('finish_weight');
        $company_id = $request->input('company_id');




        $companyexport = new DetailsExports();
        $companyexport->finish_des = $finish_des;
        $companyexport->finish_qua = $finish_qua;
        $companyexport->finish_weight = $finish_weight;
        $companyexport->created_by = $user_id;
        $companyexport->save();


        $company = CompanyDetails::find($company_id);
        if ($company) {
            $company->finish_des = $finish_des;
            $company->finish_qua = $finish_qua;
            $company->finish_weight = $finish_weight;
            $company->created_by = $user_id;
            $company->save();
        }

        return response()->json(['http_status'=>'success','message'=>'company saved successfully','company'=>$companyexport
        ]);

    }

//----------------------------------------------DECLARATION---------------------------------------------

    public function saveCompanydecla(Request $request){
        $declaration = $request->input('declaration');
        $user_id = $request->input('user_id');


        $companydecl = new CompanyDetails();
        $companydecl->declaration = $declaration;
        $companydecl->created_by = $user_id;
        $companydecl->save();


        return response()->json(['http_status'=>'success','message'=>'company saved successfully','company'=>$companydecl
        ]);

    }

}
