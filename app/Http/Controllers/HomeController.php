<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('Sampleapp');
    }

    public function application()
    {
        return view('Sampleapp');
    }
    public function applicationa()
    {
        return view('ApplicationA');
    }
    public function applicationb()
    {
        return view('ApplicationB');
    }
    public function applicationc()
    {
        return view('ApplicationC');
    }
    public function applicationd()
    {
        return view('ApplicationD');
    }
    public function applicatione()
    {
        return view('ApplicationE');
    }
    public function help()
    {
        return view('Help');
    }
    public function approval()
    {
        return view('Approval');
    }
    public function rejectapp()
    {
        return view('RejectApp');
    }


//-------------------------BOI Interface------------------------------------
    public function newapplication()
    {
        return view('newapplication');
    }
    public function newapplicationdashboard()
    {
        return view('NewapplicationDashboard');
    }
    public function approvedboi()
    {
        return view('ApprovedBOI');
    }
    public function rejectedboi()
    {
        return view('RejectedBOI');
    }




//---------------------------------Additional Secretary Interface------------------------------------------
    public function asnewapplication()
    {
        return view('ASNewApplication');
    }
    public function asnewapplicationdb()
    {
        return view('ASNewApplicationDB');
    }
    public function approvedas()
    {
        return view('ApprovedAS');
    }
    public function rejectedas()
    {
        return view('RejectedAS');
    }
}
