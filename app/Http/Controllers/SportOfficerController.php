<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class SportOfficerController extends Controller
{

    public function getProjectsDetails(Request $request){
        try {
            $data = Project::all();
            return response()->json(['http_status'=>'success','data'=>$data]);
        } catch (\Exception $e){
            return response($e->getMessage());
        }
    }

    public function getProjectsByID(Request  $request){
       $id = $request->input('id');
        $data = Project::where('id','=',$id)->first();
        $project_list = Project::all();
        return response()->json(['http_status'=>'success','data'=>$data,'project_list'=>$project_list]);
    }
}
