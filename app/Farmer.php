<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Farmer extends Model
{
    use softDeletes;
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function farms(){
        return $this->hasMany('App\Farm');
    }

    public function companies(){
        return $this->hasMany('App\FarmerHasCompany');
    }

    public function groups(){
        return $this->hasMany('App\FarmerHasGroup');
    }
}
