<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailsImportsLocal extends Model
{
    use SoftDeletes;
    protected $table = 'details_imports_local';
}
