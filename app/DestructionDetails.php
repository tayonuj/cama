<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DestructionDetails extends Model
{
    use SoftDeletes;
    protected $table = 'destruction_details';
}
