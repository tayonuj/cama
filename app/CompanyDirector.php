<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyDirector extends Model
{
    use SoftDeletes;
    protected $table = 'company_director';
}
