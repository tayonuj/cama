<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempEmployee extends Model
{
    protected $table = 'temp_employees';
}
