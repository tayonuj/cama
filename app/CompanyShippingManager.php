<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyShippingManager extends Model
{
    use SoftDeletes;
    protected $table = 'company_shipping_manager';
}
