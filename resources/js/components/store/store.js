import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    gmap:null,
    httpheader:{},
    districtData:null,
    kmlList:[],
    search:'',
    searchBar:false,
    statBar:false,
    statCard:false,

    selectedGISLayers:[],
    legendPanel:false,
    analysePanel:false,
    userBarPanel:false,


    crops:[],
    geocode:{},

    mapContextMenu:{
      x:0,
      y:0,
      visible:false
    },


    selectedLocation:null,
    currentGNDData:null,
    getAllTempFarmers:[],
    stats:{},
    gisLayers:null,
    step:1
  },
  mutations: {


  setStepper(state,payload){
      state.step = payload;
  },

  },
  getters: {
    gmap: state => state.gmap,
    httpheader: state => state.httpheader,
    step: state => state.step,

  }
});
