/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
import router from "./router";
window.Vue = require('vue');
//-----------------------Vuetify install---------------------------------------
import Vuetify from 'vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import * as VueGoogleMaps from 'vue2-google-maps';
import _ from 'lodash';
import * as geolib from 'geolib';
import VuetifyNumberInput from '@jzolago/vuetify-number-input'
import ImgInputer from 'vue-img-inputer'
import 'vue-img-inputer/dist/index.css'
import VueMask from 'v-mask'
import VueResource from 'vue-resource';
import rcolor from 'rcolor';
window.shapefile = require('shapefile');
Object.defineProperty(Vue.prototype, '$_', { value: _ });
import moment from 'moment'
import VueLazyLoad from 'vue-lazyload'
import VueConfirmDialog from 'vue-confirm-dialog'
Vue.use(geolib);
Vue.use(Vuetify);
Vue.use(VuetifyNumberInput);
Vue.component('ImgInputer', ImgInputer);
Vue.use(VueResource);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDezjCWJUVTqadDyI5wM42y0dXwEDIBdHw',
        // key: 'AIzaSyAQb-eQu_uTMovX7lpNN8o-ity8uUc3B94',
        libraries: 'drawing,language=si,geometry,places,visualization', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },
});

Vue.use(VueMask);
Vue.use(rcolor);
Vue.prototype.moment = moment;
Vue.use(VueLazyLoad);
Vue.use(VueConfirmDialog)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)
// Vue.use(VuetifyGoogleAutocomplete, {
//     apiKey: 'AIzaSyAQb-eQu_uTMovX7lpNN8o-ity8uUc3B94', // Can also be an object. E.g, for Google Maps Premium API, pass `{ client: <YOUR-CLIENT-ID> }`
// });
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);


Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('apilogin', require('./components/APILogin.vue').default);
Vue.component('application', require('./components/Application.vue').default);
Vue.component('applicationa', require('./components/ApplicationA.vue').default);
Vue.component('applicationb', require('./components/ApplicationB.vue').default);
Vue.component('applicationc', require('./components/ApplicationC.vue').default);
Vue.component('applicationd', require('./components/ApplicationD.vue').default);
Vue.component('applicatione', require('./components/ApplicationE.vue').default);
Vue.component('approval', require('./components/Approval.vue').default);
Vue.component('help', require('./components/Help.vue').default);
Vue.component('rejectapp', require('./components/RejectApp.vue').default);


//-------------------------BOI Interface------------------------------------
Vue.component('newapplication', require('./components/BOI/newapplication.vue').default);
Vue.component('newapplicationdashboard', require('./components/BOI/NewapplicationDashboard.vue').default);
Vue.component('approvedboi', require('./components/BOI/ApprovedBOI.vue').default);
Vue.component('rejectedboi', require('./components/BOI/RejectedBOI.vue').default);



//---------------------------------Additional Secretary Interface------------------------------------------
Vue.component('asnewapplication', require('./components/AdditionalSecretary/ASNewApplication.vue').default);
Vue.component('asnewapplicationdb', require('./components/AdditionalSecretary/ASNewApplicationDB.vue').default);
Vue.component('approvedas', require('./components/AdditionalSecretary/ApprovedAS.vue').default);
Vue.component('rejectedas', require('./components/AdditionalSecretary/RejectedAS.vue').default);




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify(
        {
            theme: {
                dark: false,
            },
        }
    ),
});


