import Vue from "vue";
import Router from "vue-router";
import ProjectCreation from "./components/admin/ProjectCreation";
import Home from "./components/admin/Home";
import APILogin from "./components/admin/APILogin";
import AllProjectview from "./components/admin/AllProjectview";
import ProjectPlanPopup from "./components/admin/ProjectPlanPopup";
import AssignSportOfficers from "./components/admin/AssignSportOfficers";
import SportsOfficerPage from "./components/admin/SportsOfficerPage";
import ViewMoreProjectDetails from "./components/admin/ViewMoreProjectDetails";
import AllProjectDetailsViewandEdit from "./components/admin/AllProjectDetailsViewandEdit";
import ProjectTasks from "./components/admin/ProjectTasks";
import AddProjectTask from "./components/admin/AddProjectTask";
import AddSportOfficer from "./components/admin/AddSportOfficer";
import AddNewOfficers from "./components/admin/AddNewOfficers";
import Dashboard from "./components/SportOfficer/Dashboard";
import SportOfficerProfile from "./components/SportOfficer/SportOfficerProfile";
import SportOfiicerAssignedProjectDetails from "./components/SportOfficer/SportOfiicerAssignedProjectDetails";
import ViewmoreAssignedProjectsdetails from "./components/SportOfficer/ViewmoreAssignedProjectsdetails";
import ViewRutePlanAndTimePlan from "./components/SportOfficer/ViewRutePlanAndTimePlan";
import AddCoachDeatils from "./components/SportOfficer/AddCoachDeatils";
import AssignCoach from "./components/SportOfficer/AssignCoach";
import ProjectSubmissionSo from "./components/SportOfficer/ProjectSubmissionSo";
import ProjectMonitoring from "./components/SportOfficer/ProjectMonitoring";
import SportOfficerProjectsEx from "./components/SportOfficer/SportOfficerProjectsEx";
import DistrictLevelHomePage from './components/admin/DistrictLevelHomePage';
import DivisionLevelHomePage from './components/admin/DivisionLevelHomePage';

Vue.use(Router);



const routes = [


    {
        path: "/admin",
        component: Home
    },
    {
        path: "/admin/projectcreation",
        component: ProjectCreation
    },
    {
        path: "/admin/projectcreation/:id",
        component: ProjectCreation
    },

    {
        path: "/Apilogin",
        component: APILogin
    },

    {
        path: "/sportofficer/dashboard",
        component: Dashboard

    },

    {
        path: "/admin/allprojectview",
        component: AllProjectview
    },

    {
        path: "/admin/allnewprojects",
        component: Home
    },

    {
        path: "/admin/projectplanpopup",
        component: ProjectPlanPopup
    },


    {
        path: "/admin/sportofficerpage",
        component: SportsOfficerPage
    },

    {
        path: "/admin/all_project_view_and_edit",
        component: AllProjectDetailsViewandEdit
    },

    {
        path: "/admin/add_new_officer",
        component: AddNewOfficers
    },

    {
        path: "/admin/project_task",
        component: ProjectTasks
    },

    {
        path: "/admin/district_level_home",
        component: DistrictLevelHomePage
    },
    {
        path: "/admin/dsd_level_home",
        component: DivisionLevelHomePage
    },
    


    {
        name:'assignSportOfficers',
        path: "/admin/assignSportOfficers",
        component: AssignSportOfficers

    },


    {

        path: "/admin/add_project_task",
        component: AddProjectTask

    },

    {

        path: "/admin/assign_officer",
        component: AssignSportOfficers

    },


    {

        path: "/admin/project_plan_popup",
        component: ProjectPlanPopup

    },



    {
        path: "/admin/viewmoreprojectdetails/:id",
        name:'viewmoreprojectdetails',
        component: ViewMoreProjectDetails,
        props:true,
    },

    {
        path: "/admin/addsportofficer/:id",
        name:'addsportofficer',
        component: AddSportOfficer,
        props:true,
    },

    {

        path: "/sportOfficer/profile",
        component: SportOfficerProfile

    },

    {

        path: "/sportOfficer/project_monitoring",
        component: ProjectMonitoring

    },

    {

        path: "/sportOfficer/add_coach_details",
        name:'add_coach_details',
        component: AddCoachDeatils

    },
    {

        path: "/sportOfficer/assign_coach/:index_number",
        name: 'assign_coach',
        component: AssignCoach,
        props:true,

    },

    {

        path: "/sportOfficer/assigned_project_details",
        component: SportOfiicerAssignedProjectDetails

    },

    {

        path: "/sportOfficer/view_more_assigned_project_details/:id",
        component: ViewmoreAssignedProjectsdetails,
        name: 'view_more_assigned_project_details',
        props:true,


    },

    {

        path: "/sportOfficer/view_route_plan_and_time_plan",
        component: ViewRutePlanAndTimePlan

    },

    {

        path: "/sportOfficer/project_submission",
        component: ProjectSubmissionSo

    },

    {

        path: "/sportOfficer/assign_project_Ex",
        component: SportOfficerProjectsEx

    },

    {

        path: "/sportOfficer",
        component: SportOfficerProjectsEx

    },






];

export default new Router({
    state:{
        userIsAuthorized:true,
    },
    mode: "history",
    routes
});
