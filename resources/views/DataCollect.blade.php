<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="pane scroller">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DATA COLLECTING APP</title>
    <link href="{{asset('/css/app.css') }}" rel="stylesheet">
    <style>
        a[href^="http://maps.google.com/maps"]{display:none !important}
        a[href^="https://maps.google.com/maps"]{display:none !important}
        .gmnoprint a, .gmnoprint span, .gm-style-cc {
            display:none;
        }
        /* html {   overflow-y: hidden !important; }  */

        .scroller {
            scrollbar-color: #2b2b2b;
            scrollbar-width: thin;
        }
        .scroller::-webkit-scrollbar {
            width: 10px;
            border: none !important;
        }
        .scroller::-webkit-scrollbar-track {
            background: #9E9E9E;
            border: none !important;
        }
        .scroller::-webkit-scrollbar-thumb {
            background: #2b2b2b;
            border: none !important;
        }
    </style>
    <script>
        window.Laravel = '{!! json_encode([
                'csrfToken' => csrf_token(),
                'user' => Auth::user(),
            ]) !!}';
        window.setLogout = function () {
            document.getElementById('logout-form').submit();
        };
    </script>
</head>
<body>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<div id="app">
   <data-collect></data-collect>
</div>
</body>
<script src="{{asset('/js/app.js')}}"></script>
</html>
