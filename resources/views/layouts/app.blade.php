<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
{{--    <link rel="icon" href="img/covid_icon.png" type="image">--}}

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
{{--    <script src="{{asset('js/bootstrap.js')}}"></script>--}}

    {{--    <script src="https://js.pusher.com/5.1/pusher.min.js"></script>--}}

    <script>
{{--        window.csrfToken = '{{  csrf_token() }}';--}}
        {{--window.Laravel = "'{!! json_encode(['csrfToken' => csrf_token()]) !!}'";--}}

    window.Laravel = '{!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => Auth::user()?Auth::user():'guest',
            'api_token' => (Auth::user()) ? Auth::user()->api_token : csrf_token()
        ]) !!}';

        window.setLogout = function () {
                {{--window.location.href = "{{route('admin')}}";--}}
                document.getElementById('logout-form').submit();
        };
        // window.pusher = new Pusher('249e32d26ef33e7a09d4', {
        //     cluster: 'ap2',
        //     forceTLS: false
        // });
        // Pusher.logToConsole = true;

    </script>

    {{--    //*******************************************************************************************************************--}}
    {{--    //------------------------vuetify dialog movable function----------------------------------------------------------------------}}
    {{--    //*******************************************************************************************************************    --}}
    <script>
        (function () { // make vuetify dialogs movable
            const d = {};
            document.addEventListener("mousedown", e => {
                const closestDialog = e.target.closest(".v-dialog.v-dialog--active");
                if (e.button === 0 && closestDialog != null && e.target.classList.contains("v-card__title")) { // element which can be used to move element
                    d.el = closestDialog; // element which should be moved
                    d.mouseStartX = e.clientX;
                    d.mouseStartY = e.clientY;
                    d.elStartX = d.el.getBoundingClientRect().left;
                    d.elStartY = d.el.getBoundingClientRect().top;
                    d.el.style.position = "fixed";
                    d.el.style.margin = 0;
                    d.oldTransition = d.el.style.transition;
                    d.el.style.transition = "none"
                }
            });
            document.addEventListener("mousemove", e => {
                if (d.el === undefined) return;
                d.el.style.left = Math.min(
                    Math.max(d.elStartX + e.clientX - d.mouseStartX, 0),
                    window.innerWidth - d.el.getBoundingClientRect().width
                ) + "px";
                d.el.style.top = Math.min(
                    Math.max(d.elStartY + e.clientY - d.mouseStartY, 0),
                    window.innerHeight - d.el.getBoundingClientRect().height
                ) + "px";
            });
            document.addEventListener("mouseup", () => {
                if (d.el === undefined) return;
                d.el.style.transition = d.oldTransition;
                d.el = undefined
            });
            setInterval(() => { // prevent out of bounds
                const dialog = document.querySelector(".v-dialog.v-dialog--active");
                if (dialog === null) return;
                dialog.style.left = Math.min(parseInt(dialog.style.left), window.innerWidth - dialog.getBoundingClientRect().width) + "px";
                dialog.style.top = Math.min(parseInt(dialog.style.top), window.innerHeight - dialog.getBoundingClientRect().height) + "px";
            }, 100);
        })();
    </script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--map load-->
{{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQb-eQu_uTMovX7lpNN8o-ity8uUc3B94&libraries=drawing,language=si,mgeometry,geometry,places"></script>--}}
<style>
    html{
        /*overflow-y: hidden;*/
        height: 100%;
        font-size:14px !important;
        scrollbar-face-color: #646464;
        scrollbar-base-color: #646464;
        scrollbar-3dlight-color: #646464;
        scrollbar-highlight-color: #646464;
        scrollbar-track-color: #000;
        scrollbar-arrow-color: #000;
        scrollbar-shadow-color: #646464;
        scrollbar-dark-shadow-color: #646464;
    }
    label{
        font-size:12px !important;
    }
    ::-webkit-scrollbar { width: 4px; height: 3px;}
    /*::-webkit-scrollbar-button {  background-color: #666; }*/
    ::-webkit-scrollbar-track {  background-color: #646464;}
    ::-webkit-scrollbar-track-piece { background-color: #000;}
    ::-webkit-scrollbar-thumb { height: 50px; background-color: #666; border-radius: 3px;}
    ::-webkit-scrollbar-corner { background-color: #646464;}
    ::-webkit-resizer { background-color: #666;}
    .v-text-field input {
        font-size: 0.8em !important;
    }
</style>
</head>
<body>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<v-app id="app">
        <main  style="overflow:hidden !important;">
            @yield('content')
        </main>
</v-app>

</body>

</html>
