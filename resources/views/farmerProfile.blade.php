 <!Doctype html>
<html lang="en">
<head>
    <!-- Basic Page Needs==================================================-->
    <title>GEOBIZZ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="img/we.ico"  type="image/x-icon">
    <!-- CSS==================================================-->
    <link rel="stylesheet" href="{{asset('DataCollect/assets/plugins/css/plugins.css')}}">
    <link href="{{asset('DataCollect/assets/css/style.css')}}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault" href="{{asset('DataCollect/assets/css/colors/green-style.css')}}"> </head>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQb-eQu_uTMovX7lpNN8o-ity8uUc3B94&libraries=places,drawing,mgeometry,geometry,places"async defer></script>
    <script src="{{asset('DataCollect/js/Data_view.js')}}" type="text/javascript"></script>

    <!-- Resources -->
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

    <style>
    ::-webkit-scrollbar {
        width: 0.9em;
        height: 0.9em;
    }
    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: transparent;
        -webkit-border-radius: 16px;
    }
    ::-webkit-scrollbar-thumb {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        -webkit-border-radius: 16px;
    }
    .gm-style-iw {
        width: 442px !important;
        background-image: -webkit-linear-gradient(top, rgb(223,223,223) 0%, rgb(255,255,255) 10%) !important;
        border-radius:0px !important;
    }
    #map {
        height: 500px;
        width: 100%;
        margin-top: 25px;
    }
    #chartdiv {
        width: 100%;
        height: 500px;
    }
    element.style {
        display: none !important;
    }
    .btn-default{
        width: 100% !important;
        font-weight: bolder;
        height: 10px !important;
        text-align: left !important;
        line-height: 1px !important;
        font-size: 10px !important;
    }
    .verified-action{
        width: 100% !important;
        font-weight: bolder;
        text-align: center !important;
        font-size: 10px !important;
    }
</style>

<input type="hidden" name="on_off_famerData" id="on_off_famerData" value="{{$farmer->id}}">
<body class="nav-md" onload="initMap('{{$farmer->lat}}','{{$farmer->lng}}');">



<div class="Loader"></div>
<div class="wrapper">
    <!-- Title Header Start -->
    <section class="inner-header-title" style="background-image: url('{{asset('DataCollect/uploads/pa1.jpg')}}')">
        <div class="container">
            <h1 style="font-size: 38px">FARMER PROFILE</h1>
        </div>
    </section>

    <section class="detail-desc" >
        <div class="container">
            <div class="ur-detail-wrap top-lay" style=" border-radius: 0px !important;">
                <div class="ur-detail-box">
                    <div class="ur-thumb">
                        <img src="{{$farmer->image}}" class="img-responsive" alt="" style="height: 130px">
                    </div>
                    <div class="ur-caption">
                        <h4 class="ur-title">Farmer personal data collection</h4>
                        <p class="ur-location"><i class="ti-location-pin mrg-r-5"></i>{{$farmer->name}}</p>
                        <p class="ur-location"><i class="fa fa-phone"></i>&nbsp;Tel - {{$farmer->mobile}}</p>
                        <p class="ur-location"><i class="fa fa-envelope"></i>&nbsp;{{$farmer->email}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="full-detail-description full-detail gray-bg" style="margin-top: 4%">
        <div class="container">
            <div class="ur-detail-wrap top-lay" style=" border-radius: 0px !important;">
                <div class="ur-detail-box">
                    <form class="form-horizontal tasi-form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Registration Code</div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->farmer_id}}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Farmer Name </div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->name}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Province</div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->province}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">District </div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->district}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">DS Division </div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->dsd}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">GN Division </div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->gnd}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Extend</div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">25</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">NIC Number </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->nic}}</label>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Mobile Number </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">+094{{$farmer->mobile}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Email Address </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->email}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Farmer Type </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default"></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Member of farming GP </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->group_name}}</label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Member of farming company </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->company_name}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Latitude </div> </label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->lat}}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 col-sm-5 control-label"><div class="verified-action">Longitude </div></label>
                                    <div class="col-sm-7">
                                        <label class="btn btn-default">{{$farmer->lng}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>








    <section class="full-detail-description full-detail gray-bg" style="margin-top: -8%">
        <div class="container">
            <div class="item-click">
                <article>
                    <div class="brows-job-list" style=" border-radius: 0px !important;">
                        <div class="col-md-6 col-sm-6">
                            <div class="item-fl-box">
                                <div class="brows-job-position">
                                    <h3 style="font-size:17px;">Detail view of farmers and their cultivations</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="ur-detail-wrap top-lay" style=" border-radius: 0px !important;margin-top: 7%;">
                <div class="ur-detail-box">
                    <div style="display:none;margin:0auto;" class="html5gallery" data-skin="light" data-height="380" data-slideshadow="true" data-resizemode="fill">
                        <ul class="amazingslider-thumbnails" style="display:none;">
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (19).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (71).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (54).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (16).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (7).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (33).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (36).jpg')}}"></li>
                        </ul>
                        <ul class="amazingslider-slides" style="display:none;">
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (19).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (71).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (54).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (16).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (7).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (33).jpg')}}"></li>
                            <li><img src="{{asset('DataCollect/farmer_data_uploads/Image_1 (36).jpg')}}"></li>
                        </ul>
                    </div>
                </div>
                <div class="ur-detail-box">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </section>


    <section class="full-detail-description full-detail gray-bg" style="margin-top: -7%;">
        <div class="container">
            <div class="item-click">
                <article>
                    <div class="brows-job-list" style=" border-radius: 0px !important;">
                        <div class="col-md-6 col-sm-6">
                            <div class="item-fl-box">
                                <div class="brows-job-position">
                                    <h3>Farmer crop details  (2019 to 2020)</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="ur-detail-wrap top-lay" style=" border-radius: 0px !important;margin-top: 7%;">
                <div class="card-body">
                    <article class="advance-search-job">
                        <div class="row no-mrg">
                            <div class="col-md-2 col-sm-2">
                                <div class="ur-thumb">
                                    <img src="{{asset('DataCollect/uploads/cr1.jpg')}}" class="img-responsive" alt="" style="height: 130px">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="advance-search-caption">
                                    <a href="#" title="Job Dtail"><h4>Serial number-01</h4></a>
                                    <span>Main crop - Vegetable</span><br>
                                    <span>Sub crop - carrot</span><br>
                                    <span>Plan date - 2019-11-18</span><br>
                                    <span>Expected date - 2020-02-25</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="advance-search-job-locat">
                                    <p><i class="fa fa-edit"></i>Expected yield - 100KG</p>
                                    <p><i class="fa fa-edit"></i>Expected Price - 52.00</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <a href="#"  data-toggle="modal" data-target="#" class="btn advance-search" title="apply">Edit</a>
                            </div>
                        </div>
                    </article>


                    <article class="advance-search-job">
                        <div class="row no-mrg">
                            <div class="col-md-2 col-sm-2">
                                <div class="ur-thumb">
                                    <img src="{{asset('DataCollect/uploads/p4.jpg')}}" class="img-responsive" alt="" style="height: 130px">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="advance-search-caption">
                                    <a href="#" title="Job Dtail"><h4>Serial number-02</h4></a>
                                    <span>Main crop - Paddy</span><br>
                                    <span>Sub crop - Nadu</span><br>
                                    <span>Plan date - 2019-08-08</span><br>
                                    <span>Expected date - 2020-12-15</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="advance-search-job-locat">
                                    <p><i class="fa fa-edit"></i>Expected yield - 1200KG</p>
                                    <p><i class="fa fa-edit"></i>Expected Price - 48.00</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <a href="#"  data-toggle="modal" data-target="#" class="btn advance-search" title="apply">Edit</a>
                            </div>
                        </div>
                    </article>

                    <article class="advance-search-job">
                        <div class="row no-mrg">
                            <div class="col-md-2 col-sm-2">
                                <div class="ur-thumb">
                                    <img src="{{asset('DataCollect/uploads/c3.jpg')}}" class="img-responsive" alt="" style="height: 130px">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="advance-search-caption">
                                    <a href="#" title="Job Dtail"><h4>Serial number-03</h4></a>
                                    <span>Main crop - Cabbage</span><br>
                                    <span>Sub crop - Brinjal</span><br>
                                    <span>Plan date - 2019-12-24</span><br>
                                    <span>Expected date - 2020-03-12</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="advance-search-job-locat">
                                    <p><i class="fa fa-edit"></i>Expected yield - 120KG</p>
                                    <p><i class="fa fa-edit"></i>Expected Price - 45.00</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <a href="#"  data-toggle="modal" data-target="#" class="btn advance-search" title="apply">Edit</a>
                            </div>
                        </div>
                    </article>

                    <article class="advance-search-job">
                        <div class="row no-mrg">
                            <div class="col-md-2 col-sm-2">
                                <div class="ur-thumb">
                                    <img src="{{asset('DataCollect/uploads/JPEG_20180116_164920_644234182.jpg')}}" class="img-responsive" alt="" style="height: 130px">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="advance-search-caption">
                                    <a href="#" title="Job Dtail"><h4>Serial number-04</h4></a>
                                    <span>Main crop - Fruit</span><br>
                                    <span>Sub crop - Pineapple</span><br>
                                    <span>Plan date - 2019-10-28</span><br>
                                    <span>Expected date - 2020-03-11</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="advance-search-job-locat">
                                    <p><i class="fa fa-edit"></i>Expected yield - 30KG</p>
                                    <p><i class="fa fa-edit"></i>Expected Price - 65.00</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <a href="#"  data-toggle="modal" data-target="#" class="btn advance-search" title="apply">Edit</a>
                            </div>
                        </div>
                    </article>

                    <div class="row">
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="full-detail-description full-detail gray-bg" style="margin-top: -7%;">
        <div class="container">
            <div class="item-click">
                <article>
                    <div class="brows-job-list" style=" border-radius: 0px !important;">
                        <div class="col-md-6 col-sm-6">
                            <div class="item-fl-box">
                                <div class="brows-job-position">
                                    <h3 style="font-size:17px;">Progress analysis</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="ur-detail-wrap top-lay" style=" border-radius: 0px !important;margin-top: 7%;">
                <div class="ur-detail-box">
                    <div class="hero-section-wrap fl-wrap">
                        <form class="form-horizontal">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="item-fl-box">
                                        <div class="brows-job-position">
                                            <h3 style="font-size:17px;">Month vise total quantity analysis</h3>
                                        </div>
                                    </div>
                                    <div id="chartContainer_monthvise" style="width:100%;height:480px;"></div>
                                    <script>
                                        am4core.ready(function() {

                                            // Themes begin
                                            am4core.useTheme(am4themes_animated);
                                            // Themes end

                                            // Create chart instance
                                            var chart = am4core.create("chartContainer_monthvise", am4charts.XYChart3D);

                                            // Add data
                                            chart.data = [{
                                                "country": "January",
                                                "visits": 325
                                            }, {
                                                "country": "February",
                                                "visits": 282
                                            }, {
                                                "country": "March",
                                                "visits": 300
                                            }, {
                                                "country": "April",
                                                "visits": 950
                                            }, {
                                                "country": "May",
                                                "visits": 810
                                            }];

                                            // Create axes
                                            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                            categoryAxis.dataFields.category = "country";
                                            categoryAxis.renderer.labels.template.rotation = 270;
                                            categoryAxis.renderer.labels.template.hideOversized = false;
                                            categoryAxis.renderer.minGridDistance = 20;
                                            categoryAxis.renderer.labels.template.horizontalCenter = "right";
                                            categoryAxis.renderer.labels.template.verticalCenter = "middle";
                                            categoryAxis.tooltip.label.rotation = 270;
                                            categoryAxis.tooltip.label.horizontalCenter = "right";
                                            categoryAxis.tooltip.label.verticalCenter = "middle";

                                            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                            valueAxis.title.text = "Total KG";
                                            valueAxis.title.fontWeight = "bold";

                                            // Create series
                                            var series = chart.series.push(new am4charts.ColumnSeries3D());
                                            series.dataFields.valueY = "visits";
                                            series.dataFields.categoryX = "country";
                                            series.name = "Visits";
                                            series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                                            series.columns.template.fillOpacity = .8;

                                            var columnTemplate = series.columns.template;
                                            columnTemplate.strokeWidth = 2;
                                            columnTemplate.strokeOpacity = 1;
                                            columnTemplate.stroke = am4core.color("#FFFFFF");

                                            columnTemplate.adapter.add("fill", function(fill, target) {
                                                return chart.colors.getIndex(target.dataItem.index);
                                            })

                                            columnTemplate.adapter.add("stroke", function(stroke, target) {
                                                return chart.colors.getIndex(target.dataItem.index);
                                            })

                                            chart.cursor = new am4charts.XYCursor();
                                            chart.cursor.lineX.strokeOpacity = 0;
                                            chart.cursor.lineY.strokeOpacity = 0;

                                        }); // end am4core.ready()
                                    </script>
                                </div>
                                <div class="col-md-6">
                                    <div class="item-fl-box">
                                        <div class="brows-job-position">
                                            <h3 style="font-size:17px;">Cultivated main crop analysis</h3>
                                        </div>
                                    </div>
                                    <div id="chartContainer_CultivatedCrop" style="width:100%;height:480px;"></div>
                                    <script>
                                        am4core.ready(function() {

                                            // Themes begin
                                            am4core.useTheme(am4themes_animated);
                                            // Themes end

                                            // Create chart instance
                                            var chart = am4core.create("chartContainer_CultivatedCrop", am4charts.PieChart);

                                            // Add data
                                            chart.data = [{
                                                "country": "Vegetable",
                                                "litres": 550
                                            }, {
                                                "country": "Fruits",
                                                "litres": 201
                                            }, {
                                                "country": "Paddy",
                                                "litres": 500
                                            }];

                                            // Add and configure Series
                                            var pieSeries = chart.series.push(new am4charts.PieSeries());
                                            pieSeries.dataFields.value = "litres";
                                            pieSeries.dataFields.category = "country";
                                            pieSeries.innerRadius = am4core.percent(50);
                                            pieSeries.ticks.template.disabled = true;
                                            pieSeries.labels.template.disabled = true;

                                            var rgm = new am4core.RadialGradientModifier();
                                            rgm.brightnesses.push(-0.8, -0.8, -0.5, 0, - 0.5);
                                            pieSeries.slices.template.fillModifier = rgm;
                                            pieSeries.slices.template.strokeModifier = rgm;
                                            pieSeries.slices.template.strokeOpacity = 0.4;
                                            pieSeries.slices.template.strokeWidth = 0;

                                            chart.legend = new am4charts.Legend();
                                            chart.legend.position = "right";

                                        }); // end am4core.ready()
                                    </script>
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="item-fl-box">
                                        <div class="brows-job-position">
                                            <h3 style="font-size:17px;">Month vise price analysis (Individual crop)</h3>
                                        </div>
                                    </div>
                                    <div id="chartContainer_PriceAnalysis" style="width:100%;height:480px;"></div>
                                    <script>
                                        am4core.ready(function() {

                                            // Themes begin
                                            am4core.useTheme(am4themes_animated);
                                            // Themes end

                                            // Create chart instance
                                            var chart = am4core.create("chartContainer_PriceAnalysis", am4charts.XYChart);

                                            // Add data
                                            chart.data = [{
                                                "year": "January",
                                                "Carrot": 55,
                                                "Brinjal": 25,
                                                "Nadu": 70,
                                                "Pineapple":53
                                            }, {
                                                "year": "February",
                                                "Carrot": 42,
                                                "Brinjal": 37,
                                                "Nadu": 72,
                                                "Pineapple":53
                                            }, {
                                                "year": "March",
                                                "Carrot": 46,
                                                "Brinjal": 45,
                                                "Nadu": 58,
                                                "Pineapple":73
                                            }, {
                                                "year": "April",
                                                "Carrot": 72,
                                                "Brinjal": 67,
                                                "Nadu": 69,
                                                "Pineapple":63
                                            }, {
                                                "year": "May",
                                                "Carrot": 42,
                                                "Brinjal": 38,
                                                "Nadu": 76,
                                                "Pineapple":80
                                            }];

                                            // Create category axis
                                            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                            categoryAxis.dataFields.category = "year";
                                            categoryAxis.renderer.opposite = true;

                                            // Create value axis
                                            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                            valueAxis.renderer.inversed = true;
                                            valueAxis.title.text = "Place taken";
                                            valueAxis.renderer.minLabelPosition = 0.01;

                                            // Create series
                                            var series1 = chart.series.push(new am4charts.LineSeries());
                                            series1.dataFields.valueY = "Carrot";
                                            series1.dataFields.categoryX = "year";
                                            series1.name = "Carrot";
                                            series1.strokeWidth = 3;
                                            series1.bullets.push(new am4charts.CircleBullet());
                                            series1.tooltipText = "Carrot Price in {categoryX}: RS: {valueY}";
                                            series1.legendSettings.valueText = "{valueY}";
                                            series1.visible  = false;

                                            var series2 = chart.series.push(new am4charts.LineSeries());
                                            series2.dataFields.valueY = "Brinjal";
                                            series2.dataFields.categoryX = "year";
                                            series2.name = 'Brinjal';
                                            series2.strokeWidth = 3;
                                            series2.bullets.push(new am4charts.CircleBullet());
                                            series2.tooltipText = "Brinjal Price in {categoryX}: RS: {valueY}";
                                            series2.legendSettings.valueText = "{valueY}";

                                            var series3 = chart.series.push(new am4charts.LineSeries());
                                            series3.dataFields.valueY = "Nadu";
                                            series3.dataFields.categoryX = "year";
                                            series3.name = 'Nadu';
                                            series3.strokeWidth = 3;
                                            series3.bullets.push(new am4charts.CircleBullet());
                                            series3.tooltipText = "Nadu Price in {categoryX}: RS: {valueY} ";
                                            series3.legendSettings.valueText = "{valueY}";


                                            var series4 = chart.series.push(new am4charts.LineSeries());
                                            series4.dataFields.valueY = "Pineapple";
                                            series4.dataFields.categoryX = "year";
                                            series4.name = 'Pineapple';
                                            series4.strokeWidth = 3;
                                            series4.bullets.push(new am4charts.CircleBullet());
                                            series4.tooltipText = "Pineapple Price in {categoryX}: RS: {valueY}";
                                            series4.legendSettings.valueText = "{valueY}";

                                            // Add chart cursor
                                            chart.cursor = new am4charts.XYCursor();
                                            chart.cursor.behavior = "zoomY";

                                            // Add legend
                                            chart.legend = new am4charts.Legend();

                                        }); // end am4core.ready()
                                    </script>
                                </div>
                                <div class="col-md-6">
                                    <div class="item-fl-box">
                                        <div class="brows-job-position">
                                            <h3 style="font-size:17px;">Month vise farmer Income & Expenses</h3>
                                        </div>
                                    </div>
                                    <div id="chartContainer_incomeExpenses" style="width:100%;height:480px;"></div>

                                    <script>
                                        am4core.ready(function() {

                                            // Themes begin
                                            am4core.useTheme(am4themes_animated);
                                            // Themes end

                                            // Create chart instance
                                            var chart = am4core.create("chartContainer_incomeExpenses", am4charts.XYChart);

                                            // Add data
                                            chart.data = [{
                                                "year": "January",
                                                "income": 13,
                                                "expenses": 12
                                            },{
                                                "year": "February",
                                                "income": 11,
                                                "expenses": 17
                                            },{
                                                "year": "March",
                                                "income": 10,
                                                "expenses": 13
                                            },{
                                                "year": "April",
                                                "income": 19,
                                                "expenses": 15
                                            },{
                                                "year": "May",
                                                "income": 14,
                                                "expenses": 15
                                            }];

                                            // Create axes
                                            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                                            categoryAxis.dataFields.category = "year";
                                            categoryAxis.numberFormatter.numberFormat = "# M";
                                            categoryAxis.renderer.inversed = true;
                                            categoryAxis.renderer.grid.template.location = 0;
                                            categoryAxis.renderer.cellStartLocation = 0.1;
                                            categoryAxis.renderer.cellEndLocation = 0.9;

                                            var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                                            valueAxis.renderer.opposite = true;

                                            // Create series
                                            function createSeries(field, name) {
                                                var series = chart.series.push(new am4charts.ColumnSeries());
                                                series.dataFields.valueX = field;
                                                series.dataFields.categoryY = "year";
                                                series.name = name;
                                                series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
                                                series.columns.template.height = am4core.percent(100);
                                                series.sequencedInterpolation = true;

                                                var valueLabel = series.bullets.push(new am4charts.LabelBullet());
                                                valueLabel.label.text = "{valueX}";
                                                valueLabel.label.horizontalCenter = "left";
                                                valueLabel.label.dx = 10;
                                                valueLabel.label.hideOversized = false;
                                                valueLabel.label.truncate = false;

                                                var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
                                                categoryLabel.label.text = "{name}";
                                                categoryLabel.label.horizontalCenter = "right";
                                                categoryLabel.label.dx = -10;
                                                categoryLabel.label.fill = am4core.color("#fff");
                                                categoryLabel.label.hideOversized = false;
                                                categoryLabel.label.truncate = false;
                                            }

                                            createSeries("income", "Income");
                                            createSeries("expenses", "Expenses");

                                        }); // end am4core.ready()
                                    </script>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer class="footer light-footer">
        <div class="row lg-menu">
            <div class="container">
                <div class="col-md-4 col-sm-4">
                    <img src="http://celata.co/geobizz_v4/cic/img/GeoBizz.png" class="img-responsive" style="height:80px;width:170px">
                </div>
                <div class="col-md-8 co-sm-8 pull-right">
                    <p style="font-size: 16px;margin-top: 50px !important;font-weight: bold">Copyright © 2020 CELATA TECH.PVT - All rights reserved</p>
                </div>
            </div>
        </div>
    </footer>




    <!-- Scripts==================================================-->
    <script src="{{asset('DataCollect/html5gallery/html5gallery.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/viewportchecker.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/bootsnav.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/datedropper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/loader.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/gmap3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataCollect/assets/plugins/js/jquery.easy-autocomplete.min.js')}}"></script>
    <script src="{{asset('DataCollect/assets/js/custom.js')}}"></script>
    <script src="{{asset('DataCollect/assets/js/jQuery.style.switcher.js')}}"></script>

    <script>
        $('#start_date').dateDropper();
    </script>
    <script>
        $('#end_date').dateDropper();
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#styleOptions').styleSwitcher();
        });
    </script>
    <script>
        function openRightMenu() {
            document.getElementById("rightMenu").style.display = "block";
        }
        function closeRightMenu() {
            document.getElementById("rightMenu").style.display = "none";
        }
    </script>

    <script>
        $(document).ready(function () {
            for (var u = document.getElementsByTagName("script"), v = "", n = 0; n < u.length; n++)
                u[n].src && u[n].src.match(/html5gallery\.js/i) && (v = u[n].src.substr(0, u[n].src.lastIndexOf("/") + 1));
            u = !1;
            if ("undefined" === typeof jQuery)
                u = !0;
            else if (n === jQuery.fn.jquery.split("."), 1 > n[0] || 1 === n[0] && 6 > n[1])
                u = !0;
            if (v) {
                var u = document.getElementsByTagName("head")[0], s = document.createElement("script");
                s.setAttribute("type", "text/javascript");
                s.readyState ? s.onreadystatechange = function () {
                    if ("loaded" === s.readyState || "complete" === s.readyState)
                        s.onreadystatechange = null, loadHtml5Gallery(v)
                } : s.onload = function () {
                    loadHtml5Gallery(v)
                };
                s.setAttribute("src", v + "");
                u.appendChild(s)
            } else
                loadHtml5Gallery(v)
        });
    </script>

</div>
</body>
</html>
